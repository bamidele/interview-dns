# Welcome to DNS(Drone navigation system)

This is an app that helps you locate a drone in a sector of the galaxy
To run the project locally. You can either run it on docker on your local machine
The project is built with [nodejs](https://nodejs.org/en/) and [express](https://expressjs.com) and we use [mocha](https://mochajs.org) and [chai](https://www.chaijs.com) for testing

## Setup to run locally (development enviroment)

1. Clone the git [repo](https://bitbucket.org/bamidele/interview-dns.git)
2. `cd interview-dns`
3. Run npm install with command `npm install`
4. To start the project run `npm start:dev`
5. To run test, run `npm test`

## Deploy on production without docker

### Tools needed

  1. node (version 10.0.0+)

### Steps

   1. install [pm2](https://pm2.keymetrics.io) globally with this command `sudo npm install -g pm2`
   2. Clone the git [repo](https://bitbucket.org/bamidele/interview-dns.git) and then `cd interview-dns`
   3. Run npm install with command `npm install`
   4. To start the project run `pm2 start process.yml` on 4 clusters

## Deploy on production docker
  
  The application can also be run with docker

### Steps

   1. Clone the git [repo](https://bitbucket.org/bamidele/interview-dns.git) `cd interview-dns`
   2. Build the docker image with  `docker image build . --tag interview-dns/local:latest`
   3. run the docker built `docker run -p 3000:3000 -e SECTOR_ID=4 -d interview-dns/local:latest` where  sectorId set when running docker image
