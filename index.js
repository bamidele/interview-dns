require('dotenv').config();
const logger = require('./commons/logger');
const express = require('express')
const bodyParser = require('body-parser');
const calculateLoc = require('./lib/cal');
const validateInput= require('./lib/validateInput');

const app = express()
const port = process.env.PORT || 3000;
const SectorID = process.env.SECTOR_ID || 2;

app.use(bodyParser.json());
app.get('/', (req, res) => res.send('Welcome to DNS'))
app.post('/location', (req, res) => {
    console.log('Got body:', req.body);
    const { x,y,z,vel} = req.body;
    //validation 
    let errorMsg  = validateInput(x,y,z,vel);
    if( errorMsg === null){
        let loc = calculateLoc(x,y,z,vel,SectorID);
        logger.info(`Calculated location is ${loc}`);
        return res.send({loc: loc});
    }
    res.status(422);
    logger.error(errorMsg);
    return res.send({errors:errorMsg})
})

app.listen(port, () => logger.info(`Dns listening on port ${port}! and sector id is ${SectorID}`))
module.exports = app;