const  calculateLoc = require('../../lib/cal');
var assert = require('assert');
describe('CalculateLoc', function() {
    it('should return the correct value when a string numeric value is passed', function() {
      let x=y=z=vel=sectorId ="2";
    
      assert.equal(calculateLoc(x,y,z,vel,sectorId), 14);
    });
    it('should return the correct value when a numeric value is passed (int) ', function() {
      let x=y=z=vel=sectorId =2;
      assert.equal(calculateLoc(x,y,z,vel,sectorId), 14);
    });
    it('should return the correct value when a numeric value is passed (float) ', function() {
      let x=y=z=vel=sectorId =2.1;
      assert.equal(calculateLoc(x,y,z,vel,sectorId), 15.33);
    });
    it('should return the correct value when a numeric value is passed (float,int ,string)', function() {
      let x=2 ;y=2.1;z="2.1"; sectorId =2.1;
      assert.equal(calculateLoc(x,y,z,vel,sectorId), 15.12);
    });

  
});