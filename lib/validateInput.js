module.exports = function (x,y,z,vel){
    let message = [];
    console.log(typeof Number(y));
    if(isNaN(Number(x))){
      message.push("Invalid input for x");
    }
    if(isNaN(Number(y))){
        message.push("Invalid input for y");
    }
    if(isNaN(Number(z))){
        message.push("Invalid input for z");
    }
    if(isNaN(Number(vel))){
        message.push("Invalid input for vel");
    }
    return message.length ===0 ? null: message;  
}