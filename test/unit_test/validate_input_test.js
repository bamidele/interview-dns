const  validateInput = require('../../lib/validateInput');
var assert = require('assert');
describe('ValidateInput', function() {
    describe("valid input", function(){
        it('should return null when the input field are valid (string)', function() {
            let x=y=z=vel ="2";
            assert.equal(validateInput(x,y,z,vel), null);
          });
          it('should return null value when a numeric value is passed (int) ', function() {
            let x=y=z=vel =2;
            assert.equal(validateInput(x,y,z,vel), null);
          });
          it('should return the correct value when a numeric value is passed (float) ', function() {
            let x=y=z=vel =2.1;
            assert.equal(validateInput(x,y,z,vel), null);
          }); 
          it('should return the correct value when a numeric value is passed (float) ', function() {
            let x =2.1,y =2,z=2.1,vel =2;
            assert.equal(validateInput(x,y,z,vel), null);
          });
    });
    describe("invalide input", function(){
         it('should return the right error message if value of x is invalid', function() {
            let x ="2.S",y ="2.S",z=2.1,vel =2;
            let result = validateInput(x,y,z,vel);
            assert.equal(result[0], "Invalid input for x");
          });
          it('should return the right error message if value of y is invalid', function() {
            let x ="2.1"; y ="2.S"; z=2.1; vel =2;
            let result = validateInput(x,y,z,vel);
            assert.equal(result[0], "Invalid input for y");
          });
          it('should return the right error message if value of z is invalid', function() {
            let x ="2.1";y ="2.1"; z="2.S"; vel =2;
            let result = validateInput(x,y,z,vel);
            assert.equal(result[0], "Invalid input for z");
          });
          it('should return the right error message if value of vel is invalid', function() {
            let x ="2.1"; y ="2.1"; z="2.1"; vel ="2S";
            let result = validateInput(x,y,z,vel);
            assert.equal(result[0], "Invalid input for vel");
          });
          it('should return the right error message', function() {
            let x =2.1; y ="2.1"; z="2.S"; vel =2;
            let result = validateInput(x,y,z,vel);
            assert.equal(result.length, 1);
            assert.equal(result[0], "Invalid input for z");
          });
          it('should return all error messages if there are more than one valid input', function() {
            let x =2.1; y ="2.S"; z="2.S";vel =2;
            let result = validateInput(x,y,z,vel);
            assert.equal(result.length, 2);
            assert.equal(result[0], "Invalid input for y");
            assert.equal(result[1], "Invalid input for z");
          });
          
    });
});