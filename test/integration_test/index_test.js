process.env.NODE_ENV = 'test';

let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../../index');
let should = chai.should();


chai.use(chaiHttp);
describe('/POST /location', () => {
    it('it should return the location', (done) => {
       let input=  {
            "x": "2",
            "y": "2",
            "z": "2",
            "vel":"2"
        }
      chai.request(server)
          .post('/location')
          .send(input)
          .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('loc').eql(14);
            done();
          })
    });

    it('it should return error if an input field is invalid', (done) => {
        let input=  {
             "x": "2.S",
             "y": "2",
             "z": "2",
             "vel":"2"
         }
       chai.request(server)
           .post('/location')
           .send(input)
           .end((err, res) => {
                 res.should.have.status(422);
                 res.body.should.be.a('object');
                 res.body.should.have.property('errors');
                 res.body.errors.should.include("Invalid input for x");
             done();
           })
     });
     it('it should return  multiple errors if more than one input fields  are invalid', (done) => {
        let input=  {
             "x": "2.S",
             "y": "2.S",
             "z": "2",
             "vel":"2"
         }
       chai.request(server)
           .post('/location')
           .send(input)
           .end((err, res) => {
                 res.should.have.status(422);
                 res.body.should.be.a('object');
                 res.body.should.have.property('errors');
                 res.body.errors.should.include("Invalid input for x");
                 res.body.errors.should.include("Invalid input for y");
             done();
           })
     });
});
